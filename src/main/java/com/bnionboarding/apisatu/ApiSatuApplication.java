package com.bnionboarding.apisatu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSatuApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSatuApplication.class, args);
	}

}
