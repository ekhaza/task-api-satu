package com.bnionboarding.apisatu.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bnionboarding.apisatu.helpers.responseData;
import com.bnionboarding.apisatu.models.hellobody;

@RestController
@RequestMapping("/api/hello")
public class helloWorld {

    @GetMapping("/{name}")
    public Map<String, Object> helloGetName(@PathVariable String name) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", name);
        Map<String, Object> result = responseData.responseSuccess(data);
        return result;
    }

    @GetMapping()
    public Map<String, Object> helloGetNameByParam(
            @RequestParam(value = "name", required = false, defaultValue = "Buddy") String name) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", name);
        Map<String, Object> result = responseData.responseSuccess(data);
        return result;
    }

    @PostMapping
    public Map<String, Object> helloPostName(@RequestBody hellobody body) {

        Map<String, Object> data = new HashMap<>();
        data.put("nama", body.getName());

        Map<String, Object> result = responseData.responseSuccess(data);
        return result;
    }
}
