package com.bnionboarding.apisatu.helpers;

import java.util.HashMap;
import java.util.Map;

public class responseData {
    public static Map<String, Object> responseSuccess(Map<String, Object> data) {
        Map<String, Object> result = new HashMap<>();
        result.put("responseCode", "200");
        result.put("payload", data);
        result.put("isSuccess", true);
        result.put("message", "Success");
        return result;
    }
}
